package clases;

/**
 * @autor Joel Capape Hern�ndez
 */
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Equipo {
	public static Scanner input = new Scanner(System.in);
	private ArrayList<Entrenador> listaEntrenadorPersonal;
	private ArrayList<Jugador> listaJugadores;

	/**
	 * Aqu� declaro los constructores que ser�n dos arraylist de jugador y
	 * entrenador
	 */
	public Equipo() {
		listaEntrenadorPersonal = new ArrayList<Entrenador>();
		listaJugadores = new ArrayList<Jugador>();
	}

	/**
	 * Metodo que me dice si existe el entrenador
	 * 
	 * @param nLicencia es el parametro que le pasamos al metodo existe Entrenador
	 *                  que contiene el n�mero de Licencia
	 * @return si la licencia ya ha sido introduida me devuelve true y sino me
	 *         devuelve false este metodo lo utilizaremos m�s adelante dentro de
	 *         otro metodo
	 */
	public boolean existeEntrenador(String nLicencia) {
		for (Entrenador entrenador : listaEntrenadorPersonal) {
			if (entrenador != null && entrenador.getnLicencia().equals(nLicencia)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param nFicha es el parametro que le pasamos al metodo existe jugador que
	 *               contiene el n�mero de ficha del jugador
	 * @return si la ficha ya ha sido introducida me devuelve true y sino me
	 *         devuelve false. Al igual que el metodo existe entrenador lo
	 *         utilizaremos dentro de otro metodo
	 */
	public boolean existeJugador(String nFicha) {
		for (Jugador jugador : listaJugadores) {
			if (jugador != null && jugador.getnFicha().equals(nFicha)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Aqu� doy de alta el Entrenador pero primeramente le paso el n�mero de
	 * licencia que lo recibira el metodo creado anteriomente, si la licencia no
	 * esta repetida me dejara introducir los demas datos del entrenador y si la
	 * licencia ya ha sido introducida no me dejara introducir nada m�s y me dira un
	 * mensaje diciendo que la licencia ya ha sido introducida
	 */
	public void altaEntrenador() {
		System.out.println("Dame el n�mero de Licencia");
		String nLicencia = input.nextLine();
		if (!existeEntrenador(nLicencia)) {
			System.out.println("Dame el nombre del entrenador");
			String nombre = input.nextLine();
			System.out.println("Dame el apellido del entrenador");
			String apellido = input.nextLine();
			System.out.println("Dime si es entrenador f�sico o t�cnico");
			String tipo = input.nextLine();
			Entrenador altaEntrenador = new Entrenador(nombre, apellido, tipo, nLicencia);
			listaEntrenadorPersonal.add(altaEntrenador);
		} else {
			System.out.println("El n�mero de licencia ya ha sido registrado anteriormente."
					+ "(No se introducir� al entrenador con la licencia repetida)");
		}
	}

	/**
	 * Aqu� listo todos los entrenadores que he dado de alta
	 */
	public void listarEntrenador() {
		for (Entrenador entrenador : listaEntrenadorPersonal) {
			if (entrenador != null) {
				System.out.println(entrenador);
			}
		}
	}

	/**
	 * Aqu� introduzco el nombre del entrenador que quiero buscar
	 * 
	 * @return devuelve un nombre que utilizo en el metodo buscar Entrenador
	 */
	public String nombreEntrenador() {
		System.out.println("Dame el nombre del entrenador para buscar");
		String nombre = input.nextLine();
		return nombre;
	}

	/**
	 * 
	 * @param nombre donde le paso el nombre al metodo buscarEntrenador
	 * @return devuelve los datos del entrenador encontrado y si no encuentra al
	 *         entrenador nos devuelve un null
	 */
	public Entrenador buscarEntrenador(String nombre) {
		for (Entrenador entrenador : listaEntrenadorPersonal) {
			if (entrenador != null && entrenador.getNombre().equals(nombre)) {
				return entrenador;
			}
		}
		return null;
	}

	/**
	 * Metodo en el que le pido un apellido
	 * 
	 * @return devuelve el apellido que lo utilizo en el metodo eliminarEntrenador
	 */
	public String introducirApellido() {
		System.out.println("Dime el apellido del entrenador que quieres eliminar");
		String apellido = input.nextLine();
		return apellido;
	}

	/**
	 * El metodo eliminar entrenador comprueba si el parametro que le paso es igual
	 * al apellido y si es igual lo elimina
	 * 
	 * @param apellido parametro que introducimos en el metodo eliminarEntrenador
	 */
	public void eliminarEntrenador(String apellido) {
		Iterator<Entrenador> iteradorEntrenador = listaEntrenadorPersonal.iterator();
		while (iteradorEntrenador.hasNext()) {
			Entrenador entrenador = iteradorEntrenador.next();
			if (entrenador.getApellido().equals(apellido)) {
				iteradorEntrenador.remove();
				System.out.println("El entrenador ha sido eliminado correctamente");
			}
		}
	}

	/**
	 * Aqu� doy de alta el Jugador pero primeramente le paso el n�mero de ficha que
	 * lo recibira el metodo creado anteriomente, si la ficha no esta repetida me
	 * dejara introducir los demas datos del jugador y si la ficha ya ha sido
	 * introducida no me dejara introducir nada m�s y me dira un mensaje diciendo
	 * que la ficha ya ha sido introducida
	 */
	public void altaJugador() {

		System.out.println("Dime el n�mero de ficha del jugador");
		String nFicha = input.nextLine();
		if (!existeJugador(nFicha)) {
			System.out.println("Dame el nombre del jugador");
			String nombre = input.nextLine();
			System.out.println("Dame el apellido del jugador");
			String apellido = input.nextLine();
			System.out.println("Dame la edad del jugador");
			int edad = input.nextInt();
			input.nextLine();
			System.out.println("La posici�n en la que puede jugar son: Portero, Defensa, Mediocentro o Delantero");
			String posicion = input.nextLine();
			System.out.println("Dame el dorsal del jugador");
			int dorsal = input.nextInt();
			System.out.println("Dime el n�mero de goles");
			int nGoles = input.nextInt();
			input.nextLine();
			Jugador altaJugador = new Jugador(nombre, apellido, edad, posicion, dorsal, nGoles, nFicha);
			listaJugadores.add(altaJugador);
		} else {
			System.out.println(
					"La ficha del jugador ya ha sido introducida(No se introducir� al jugador con la ficha repetida)");
		}
	}

	/**
	 * Metodo que me lista todos los jugadores a�adidos al arrayList
	 */
	public void listarJugadores() {
		for (Jugador jugador : listaJugadores) {
			if (jugador != null) {
				System.out.println(jugador);
			}
		}
	}

	/**
	 * Metodo que le pido la ficha del jugador que utilizo despues en el metodo
	 * listarJugadorPorPosicion
	 * 
	 * @return devuelve la variable nFicha que se la pasaremos como parametro al
	 *         metodo buscarJugador
	 */
	public String introducirFicha() {
		System.out.println("Dame el n�mero de ficha del jugador que hay que buscar");
		String nFicha = input.nextLine();
		return nFicha;
	}

	/**
	 * 
	 * @param ficha parametro que le pasamos al metodo buscarJugador
	 * @return si la ficha coincide devolvera el jugador y si el jugador no es
	 *         encontrado devolvera un null
	 */
	public Jugador buscarJugador(String ficha) {
		for (int i = 0; i < listaJugadores.size(); i++) {
			Jugador jugadorBuscar = listaJugadores.get(i);
			if (listaJugadores.size() != 0 && jugadorBuscar.getnFicha().equals(ficha)) {
				return jugadorBuscar;
			}
		}
		return null;
	}
/**
 * 
 * @return devuelve el nombre de un jugador para usar en otro metodo
 */
	
	public String introducirNombreJugador() {
		System.out.println("Dime el nombre del jugador que quieres eliminar");
		String nombre=input.nextLine();
		return nombre;
	}
	/**
	 * 
	 * @param nombre parametro que le pasamos al metodo que elimina al jugador seg�n
	 *               el nombre que recibe por parametro
	 */
	public void eliminarJugador(String nombre) {
		Iterator<Jugador> iteradorJugador = listaJugadores.iterator();
		while (iteradorJugador.hasNext()) {
			Jugador jugador = iteradorJugador.next();
			if (jugador.getNombre().equals(nombre)) {
				iteradorJugador.remove();
				System.out.println("El jugador ha sido eliminado correctamente");
			}
		}
	}

	/**
	 * Metodo en el que le pido una posicion
	 * 
	 * @return nos devuelve la posicion que se la pasaremos por parametro despues al
	 *         metodo listar Posicion
	 */
	public String introducirPosicion() {
		System.out.println("Dime la posicion que hay que listar");
		String posicion = input.nextLine();
		return posicion;
	}

	/**
	 * Metodo que nos lista los jugadores por un atributo en concreto
	 * 
	 * @param posicion parametro que le pasamos al metodo listarPosicion que nos
	 *                 devolvera los jugadores que compartan esa posicion
	 */
	public void listarPosicion(String posicion) {
		for (Jugador jugador : listaJugadores) {
			if (jugador.getPosicion().equals(posicion)) {
				System.out.println(jugador);
			}
		}
	}
/**
 * 
 * @return devuelve la ficha de un jugador para usar en otro metodo
 */
	
	public String fichaDelJugadorAsignar() {
		System.out.println("Dime el n�mero de ficha del jugador que quiere un Entrenador Personal");
		String nombre=input.nextLine();
		return nombre;
	}
	/**
	 * 
	 * @return devuelve el nombre de un entrenador para usar en otro metodo
	 */
	public String nombreDelEntrenadorAsignado() {
		System.out.println("Dime el nombre del entrenador a asignar");
		String nombre=input.nextLine();
		return nombre;
	}
	/**
	 * Metodo que asigna a un jugador un entrenador
	 * 
	 * @param fichaJugador variable que contiene el nombre del jugador
	 * @param nEntrenador variable que contiene el nombre del entrenador
	 */
	public void asignarEntrenador(String fichaJugador, String nEntrenador) {
		if (buscarJugador(fichaJugador) != null && buscarEntrenador(nEntrenador) != null) {
			Entrenador entrenador = buscarEntrenador(nEntrenador);
			Jugador jugador = buscarJugador(fichaJugador);
			jugador.setEntrenadorPersonal(entrenador);
			System.out.println("Se ha asignado correctamente un entrenador atendiendo a las car�cteristicas");
		}
	}

	/**
	 * Metodo que lista aquellos jugadores a los cuales se les ha asignado un
	 * entrenador
	 * 
	 * @param nombre variable que recoge el nombre de un entrenador
	 */
	public void listarJugadoresDeEntrenador(String nombre) {
		for (Jugador jugador : listaJugadores) {
			if (jugador.getEntrenadorPersonal() != null && jugador.getEntrenadorPersonal().getNombre().equals(nombre)) {
				System.out.println(jugador);
			}
		}
	}

	/**
	 * Metodos Extras
	 */
	/**
	 * Metodo en el que le cambio el nombre de un jugador Primero le pido el nombre
	 * antiguo del jugador al que le quiero cambiar el nombre, compruebo con un if
	 * que el nombre coincide y si coincide el introduzco el nombre nuevo
	 */
	public void cambiarNombreJugador() {
		System.out.println("Dime el nombre del jugador que quieres cambiar");
		String nombre = input.nextLine();
		for (Jugador jugador : listaJugadores) {
			if (jugador.getNombre().equals(nombre)) {
				System.out.println("Dime el nombre correcto del jugador");
				String nombreNuevo = input.nextLine();
				jugador.setNombre(nombreNuevo);
			}
		}
	}

	/**
	 * Metodo en el nos devuelve el jugador que m�s goles tiene. En este metodo he
	 * convertido en un array normal los arraylist. He declarado una variable
	 * posicion donde guardo la posicion del gol mayor que se la paso al array de
	 * nombres y as� nos devuelve el jugador que m�s goles tiene
	 */
	public void maximoGoleador() {
		int[] arrayGoles = new int[listaJugadores.size()];
		String[] nombres = new String[listaJugadores.size()];
		int mayor = 0;
		int posicion = 0;
		String nombre = "";
		for (int i = 0; i < arrayGoles.length; i++) {
			arrayGoles[i] = listaJugadores.get(i).getnGoles();
		}
		for (int i = 0; i < nombres.length; i++) {
			nombres[i] = listaJugadores.get(i).getNombre();
		}
		for (int i = 0; i < arrayGoles.length; i++) {
			if (mayor < arrayGoles[i]) {
				mayor = arrayGoles[i];
				posicion = i;
			}
		}
		for (int i = 0; i < nombres.length; i++) {
			if (posicion == i) {
				nombre = nombres[i];
			}
		}
		System.out.println("El jugador " + nombre + " ha marcado " + mayor + " goles");
	}

	/**
	 * Metodo que me muestra la media total de goles del equipo
	 */
	public void mediaGoles() {
		int suma = 0;
		double media = 0;
		for (int i = 0; i < listaJugadores.size(); i++) {
			suma = suma + listaJugadores.get(i).getnGoles();
		}
		media = (double) suma / (double) listaJugadores.size();
		System.out.println("La media de goles entre todos los jugadores es " + media);
	}

	/**
	 * Este metodo nos dice si los jugadores est�n entrenando o no. Para ello,
	 * casteo el array list en un array y si coincide la posicion del array con la
	 * variable donde guardo un Math.random() los jugadores estar�n entrenando si no
	 * no.
	 */
	public void entrenamiento() {
		Jugador[] array = new Jugador[listaJugadores.size()];
		int numeroAleatorio = (int) (Math.random() * 5);
		boolean estaEntrenando=false;
		listaJugadores.toArray(array);
		for (int i = 0; i < listaJugadores.size(); i++) {
			array[i] = listaJugadores.get(i);
		}
		for (int i = 0; i < array.length; i++) {
			if (i == numeroAleatorio) {
				estaEntrenando=true;
			} else {
				estaEntrenando=false;
			}
		}
		if(estaEntrenando) {
			System.out.println("Los jugadores est�n entrenando");
		}else {
			System.out.println("Los jugadores tienen descanso");
		}
	}

	/**
	 * 
	 * @return aqu� devuelvo las opciones del men�
	 */
	public int opcionMenu() {
		System.out.println("Selecciona la opci�n");
		int opcion = input.nextInt();
		input.nextLine();
		return opcion;
	}
}
