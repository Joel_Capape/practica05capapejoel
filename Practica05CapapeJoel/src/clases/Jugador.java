package clases;

public class Jugador {

//Declaro los atributos de la clase jugador. Aqui le paso un atributo de la clase Entrenador

	private String nombre;
	private String apellido;
	private int edad;
	private String posicion;
	private int dorsal;
	private int nGoles;
	private String nFicha;
	Entrenador entrenadorPersonal;

//Creo los constructores de la clase jugador

	public Jugador() {
		this.nombre = "";
		this.apellido = "";
		this.edad = 0;
		this.posicion = "";
		this.dorsal = 0;
		this.nGoles = 0;
	}

	public Jugador(String nombre, String apellido, int edad, String posicion, int dorsal, int nGoles) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.edad = edad;
		this.posicion = posicion;
		this.dorsal = dorsal;
		this.nGoles = nGoles;
	}

	public Jugador(String nombre, String apellido, int edad, String posicion, int dorsal, int nGoles, String nFicha) {
		this(nombre, apellido, edad, posicion, dorsal, nGoles);
		this.nFicha = nFicha;
	}

// Creo los metodos setter y getter

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getPosicion() {
		return posicion;
	}

	public void setPosicion(String posicion) {
		this.posicion = posicion;
	}

	public int getDorsal() {
		return dorsal;
	}

	public void setDorsal(int dorsal) {
		this.dorsal = dorsal;
	}

	public int getnGoles() {
		return nGoles;
	}

	public void setnGoles(int nGoles) {
		this.nGoles = nGoles;
	}

	public String getnFicha() {
		return nFicha;
	}

	public void setnFicha(String nFicha) {
		this.nFicha = nFicha;
	}

	public Entrenador getEntrenadorPersonal() {
		return entrenadorPersonal;
	}

	public void setEntrenadorPersonal(Entrenador entrenadorPersonal) {
		this.entrenadorPersonal = entrenadorPersonal;
	}

//Creo el metodo ToString()

	@Override
	public String toString() {
		return "Jugador [nombre=" + nombre + ", apellido=" + apellido + ", edad=" + edad + ", posicion=" + posicion
				+ ", dorsal=" + dorsal + ", nGoles=" + nGoles + ", nFicha=" + nFicha + ", entrenadorPersonal="
				+ entrenadorPersonal + "]";
	}

}
