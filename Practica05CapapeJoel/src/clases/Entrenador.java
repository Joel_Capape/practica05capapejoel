package clases;

//Declaro los atributos de la clase Entrenador

public class Entrenador {
	private String nombre;
	private String apellido;
	private String tipoDeEntrenador;
	private String nLicencia;

//Creo los constructores de la clase Entrenador

	public Entrenador() {
		this.nombre = "";
		this.apellido = "";
		this.tipoDeEntrenador = "";
	}

	public Entrenador(String nombre, String apellido, String tipoDeEntrenador) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.tipoDeEntrenador = tipoDeEntrenador;
	}

	public Entrenador(String nombre, String apellido, String tipoDeEntrenador, String nLicencia) {
		this(nombre, apellido, tipoDeEntrenador);
		this.nLicencia = nLicencia;
	}
//Creo los metodos setter y getter

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTipoDeEntrenador() {
		return tipoDeEntrenador;
	}

	public void setTipoDeEntrenador(String tipoDeEntrenador) {
		this.tipoDeEntrenador = tipoDeEntrenador;
	}

	public String getnLicencia() {
		return nLicencia;
	}

	public void setnLicencia(String nLicencia) {
		this.nLicencia = nLicencia;
	}
//Creo el metodo ToString()

	@Override
	public String toString() {
		return "Entrenador [nombre=" + nombre + ", apellido=" + apellido + ", tipoDeEntrenador=" + tipoDeEntrenador
				+ ", nLicencia=" + nLicencia + "]";
	}

}
