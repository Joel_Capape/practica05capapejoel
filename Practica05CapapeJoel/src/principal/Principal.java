package principal;

import clases.Equipo;

/**
 * 
 * @author Joel Capape Hern�ndez
 *
 */
public class Principal {

	public static void main(String[] args) {
		/**
		 * Creo la instancia de la clase Gestor en mi caso Equipo
		 */
		Equipo equipo = new Equipo();
		/**
		 * Doy de alta 3 elementos de la clase 1 que es Entrenador(comprueba previamente
		 * si la licencia esta repetida). Si la licencia esta repetida no te lo da de
		 * alta
		 */
		equipo.altaEntrenador();
		equipo.altaEntrenador();
		equipo.altaEntrenador();
		/**
		 * Doy de alta 4 elementos de la clase 2 que es Jugador(comprueba previamente si
		 * la ficha esta repetida). Si la ficha esta repetida no te lo da de alta
		 */
		equipo.altaJugador();
		equipo.altaJugador();
		equipo.altaJugador();
		equipo.altaJugador();
		int opcion;
		/**
		 * Aqu� creo un men� que contiene los metodos comentados anteriormente
		 */
		do {
			System.out.println("__________________________________");
			System.out.println("                 Men�             ");
			System.out.println("1.- Listar entrenadores");
			System.out.println("2.- Buscar entrenador por nombre");
			System.out.println("3.- Eliminar Entrenador");
			System.out.println("4.- Listar jugadores");
			System.out.println("5.- Buscar jugador por n�mero de ficha");
			System.out.println("6.- Eliminar jugadores");
			System.out.println("7.- Listar jugador por posici�n");
			System.out.println("8.- Listar jugadores por entrenador");
			System.out.println("9.- Asignar un entrenador a un jugador y listar");
			System.out.println("10.- Estad�sticas t�cnicas del Equipo");
			System.out.println("11.- Salir de la aplicaci�n");
			System.out.println("__________________________________");
			opcion = equipo.opcionMenu();
			switch (opcion) {
			case 1:
				equipo.listarEntrenador();
				break;
			case 2:
				String nombre = equipo.nombreEntrenador();
				System.out.println(equipo.buscarEntrenador(nombre));
				break;
			case 3:
				String apellido = equipo.introducirApellido();
				equipo.eliminarEntrenador(apellido);
				System.out.println();
				System.out.println("Comprobaci�n de que se ha eliminado el entrenador");
				equipo.listarEntrenador();
				break;
			case 4:
				equipo.listarJugadores();
				break;
			case 5:
				String ficha = equipo.introducirFicha();
				System.out.println(equipo.buscarJugador(ficha));
				break;
			case 6:
				String nombreJugador = equipo.introducirNombreJugador();
				equipo.eliminarJugador(nombreJugador);
				System.out.println("");
				System.out.println("Comprobaci�n de que se ha eliminado el jugador");
				equipo.listarJugadores();
				break;
			case 7:
				String posicion = equipo.introducirPosicion();
				equipo.listarPosicion(posicion);
				break;
			case 8:
				String nombreEntrenador = equipo.nombreDelEntrenadorAsignado();
				equipo.listarJugadoresDeEntrenador(nombreEntrenador);
				break;
			case 9:
				String numeroDeFicha = equipo.fichaDelJugadorAsignar();
				String nombreEntrenadores = equipo.nombreDelEntrenadorAsignado();
				equipo.asignarEntrenador(numeroDeFicha, nombreEntrenadores);
				System.out.println();
				System.out.println("Comprobacion de se ha asignado correctamente(Volviendo a ejecutar opci�n 8)");
				break;
			case 10:
				/**
				 * Aqu� creo un submenu que me muestra los metodos extras
				 */
				int opcion2;
				System.out.println("_____________________________________");
				System.out.println("                Men�                 ");
				System.out.println("1.- Cambiar nombre de jugador");
				System.out.println("2.- M�ximo goleador");
				System.out.println("3.- Media total de goles del Equipo");
				System.out.println("4.- �Est�n en Entrenamiento?");
				System.out.println("5.- Volver al men� principal");
				System.out.println("_____________________________________");
				do {
					opcion2 = equipo.opcionMenu();
					switch (opcion2) {
					case 1:
						equipo.cambiarNombreJugador();
						System.out.println("Comprobaci�n de que se ha actualizado el jugador");
						equipo.listarJugadores();
						break;
					case 2:
						equipo.maximoGoleador();
						break;
					case 3:
						equipo.mediaGoles();
						break;
					case 4:
						equipo.entrenamiento();
						break;
					case 5:
						System.out.println("Volviendo al men� principal ......");
						break;
					default:
						System.out.println("Opci�n no contemplada");
					}
				} while (opcion2 != 5);
				break;
			case 11:
				System.out.println("La aplicaci�n se ha cerrado correctamente");
				System.exit(0);
			default:
				System.out.println("Opci�n no contemplada");
			}
		} while (opcion != 11);
	}

}
